import { Component, OnInit } from '@angular/core';
import {Vehicle} from '../vehicle';
import {VehicleService} from '../vehicle.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {
  vehicles: Vehicle[];

  getVehicles(): void {
    this.vehicleService.getVehicles().subscribe(vehicles => this.vehicles = vehicles);
  }

  constructor(private vehicleService: VehicleService) { }

  ngOnInit() {
    this.getVehicles();
  }

  add(brand: string, name: string): void {
    name = name.trim();
    brand = brand.trim();
    if (name) {
      if (brand) {
        this.vehicleService.addVehicle({name, brand} as Vehicle).subscribe(vehicle => this.vehicles.push(vehicle));
      } else {
        return;
      }
    } else {
      return;
    }
  }

  delete(vehicle: Vehicle): void {
    this.vehicles = this.vehicles.filter(v => v !== vehicle);
    this.vehicleService.deleteVehicle(vehicle).subscribe();
  }
}
