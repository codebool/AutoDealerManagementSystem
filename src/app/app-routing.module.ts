import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VehiclesComponent} from './vehicles/vehicles.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {VehicleDetailComponent} from './vehicle-detail/vehicle-detail.component';

const routes: Routes = [
  {path: 'vehicles', component: VehiclesComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'detail/:id', component: VehicleDetailComponent}
];

@NgModule({
  exports: [RouterModule],

  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
