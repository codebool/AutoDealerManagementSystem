export interface Vehicle {
  id: number;
  name: string;
  year: number;
  brand: string;
  timeIn: string;
  ordered: boolean;
  msrp: number;
  discount: number;
  color: string;
  trim: string;
  buyerId: number;
  buyerName: string;
  buyerGender: string;
  buyerPhone: string;
  buyerEmail: string;
}

// export interface Buyer {
//   id: number;
//   name: string;
//   gender: string;
//   phone: string;
//   email: string;
// }
