import {Component, Input, OnInit} from '@angular/core';
import {Vehicle} from '../vehicle';
import {ActivatedRoute} from '@angular/router';
import {VehicleService} from '../vehicle.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './vehicle-detail.component.html',
  styleUrls: ['./vehicle-detail.component.css']
})

export class VehicleDetailComponent implements OnInit {
  @Input() vehicle: Vehicle;

  constructor(
    private route: ActivatedRoute,
    private vehicleService: VehicleService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getVehicle();
  }

  getVehicle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.vehicleService.getVehicle(id)
      .subscribe(vehicle => this.vehicle = vehicle);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.vehicleService.updateVehicle(this.vehicle).subscribe(() => this.goBack());
  }

}
