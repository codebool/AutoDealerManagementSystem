import { Component, OnInit } from '@angular/core';
import {Vehicle} from '../vehicle';
import {VehicleService} from '../vehicle.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  vehicles: Vehicle[] = [];
  sales: number;

  constructor(private vehicleService: VehicleService) {
    this.sales = 0;
  }

  ngOnInit() {
    this.getVehicles();
  }

  getVehicles(): void {
    this.vehicleService.getVehicles().subscribe(vehicles => this.vehicles = vehicles.filter(v => (v.ordered === false || v.ordered === undefined)));
  }

}
