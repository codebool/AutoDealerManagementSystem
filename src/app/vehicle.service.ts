import { Injectable } from '@angular/core';
import {Vehicle} from './vehicle';
import {Observable, of} from 'rxjs';
import {MessageService} from './message.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class VehicleService {

  private vehicleUrl = `http://localhost:3000/vehicles`;

  private log(message: string) {
    this.messageService.add(`Your action: ${message}`);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  constructor(
    private http: HttpClient,
    private messageService: MessageService) {}

  getVehicles(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(this.vehicleUrl).pipe(
      tap(vehicles => this.log('List all vehicles')),
      catchError(this.handleError(`getVehicles`, []))
    );
  }

  getVehicleNo404<Data>(id: number): Observable<Vehicle> {
    const url = `${this.vehicleUrl}/?id=${id}`;
    return this.http.get<Vehicle[]>(url).pipe(
      map(vehicles => vehicles[0]),
      tap(v => {
        const outcome = v ? `fetched` : `did not find`;
        this.log(`${outcome} vehicle id=${id}`);
      })
    );
  }

  getVehicle(id: number): Observable<Vehicle> {
    const url = `${this.vehicleUrl}/${id}`;
    return this.http.get<Vehicle>(url).pipe(
      tap(_ => this.log(`You are looking for the  vehicle id=${id}`)),
      catchError(this.handleError<Vehicle>(`getVehicle id=${id}`))
    );
  }

  addVehicle(vehicle: Vehicle): Observable<Vehicle> {
    return this.http.post<Vehicle>(this.vehicleUrl, vehicle, httpOptions).pipe(
      tap((vehicle: Vehicle) => this.log(`added vehicle id=${vehicle.id}`)),
      catchError(this.handleError<Vehicle>('addVehicle'))
    );
  }

  updateVehicle(vehicle: Vehicle): Observable<any> {
    return this.http.put(this.vehicleUrl + '/' + vehicle.id, vehicle, httpOptions).pipe(
      tap(_ => this.log(`updated vehicle id=${vehicle.id}`)),
      catchError(this.handleError<any>('updateVehicle'))
    );
  }

  deleteVehicle(vehicle: Vehicle | number): Observable<Vehicle> {
    const id = typeof vehicle === 'number' ? vehicle : vehicle.id;
    const url = `${this.vehicleUrl}/${id}`;

    return this.http.delete<Vehicle>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted vehicle id=${id}`)),
      catchError(this.handleError<Vehicle>('deleteVehicle'))
    );
  }
}
